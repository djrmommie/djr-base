<?php if ( is_active_sidebar( 'primary' ) ) : // if has sidebars. ?>

	<aside <?php hybrid_attr( 'sidebar', 'primary' ); ?>>

        <?php dynamic_sidebar( 'primary' ); // Displays the primary sidebar. ?>

	</aside><!-- #sidebar-primary -->

<?php endif; // End layout check. ?>