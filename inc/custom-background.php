<?php
/**
 * Custom background callback
 *
 * @package    DJRBase
 * @author     Jenny Ragan <jenny@jennyragan.com>
 * @copyright  Copyright (c) 2017, Jenny Ragan
 * @link       https://bitbucket.org/djrmommie/djr-base
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

function djr_base_custom_background_callback() {

	// Get the background image.
	$image = get_background_image();

	// If there's an image, just call the normal WordPress callback. We won't do anything here.
	if ( $image ) {
		_custom_background_cb();
		return;
	}

	// Get the background color.
	$color = get_background_color();

	// If no background color, return.
	if ( ! $color )
		return;

	// Use 'background' instead of 'background-color'.
	$style = "background: #{$color};";

?>
<style type="text/css" id="custom-background-css">body.custom-background { <?php echo trim( $style ); ?> }</style>
<?php

}
