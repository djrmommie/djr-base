<?php
/**
 * Functions for use within theme templates.
 *
 * @package    DJRBase
 * @author     Jenny Ragan <jenny@jennyragan.com>
 * @copyright  Copyright (c) 2017, Jenny Ragan
 * @link       https://bitbucket.org/djrmommie/djr-base
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

