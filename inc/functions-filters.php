<?php
/**
 * Hooks and filters added by theme
 *
 * @package    DJRBase
 * @author     Jenny Ragan <jenny@jennyragan.com>
 * @copyright  Copyright (c) 2017, Jenny Ragan
 * @link       https://bitbucket.org/djrmommie/djr-base
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/**
 * Registers sidebars.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function djr_base_register_sidebars() {

	hybrid_register_sidebar(
		array(
			'id'          => 'primary',
			'name'        => esc_html_x( 'Primary', 'sidebar', 'djr-base' ),
			'description' => esc_html__( 'Add sidebar description.', 'djr-base' )
		)
	);

	hybrid_register_sidebar(
		array(
			'id'          => 'subsidiary',
			'name'        => esc_html_x( 'Subsidiary', 'sidebar', 'djr-base' ),
			'description' => esc_html__( 'Add sidebar description.', 'djr-base' )
		)
	);
}
add_action( 'widgets_init', 'djr_base_register_sidebars', 5 );

/**
 * Load scripts for the front end.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function djr_base_enqueue_scripts() {

	wp_enqueue_script( 'html5', get_parent_theme_file_uri( 'js/html5shiv.min.js' ), '', '3.7.3', false );
	wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );
	wp_enqueue_script( 'djr-base-navigation', get_parent_theme_file_uri( '/js/navigation.js' ), array(), '1.0.0', true );
	wp_enqueue_script( 'djr-base-skip-link-focus-fix', get_parent_theme_file_uri( '/js/skip-link-focus-fix.js' ), array(), '1.0.0', true );
	wp_enqueue_script( 'flexibility', get_parent_theme_file_uri( 'js/flexibility.js' ), array(), '2.0.1', true );
	wp_enqueue_script( 'ofi', get_parent_theme_file_uri( 'js/ofi.min.js' ), array(), '3.1.0', true );
	wp_enqueue_script( 'djr-base', get_parent_theme_file_uri( 'js/theme.js' ), array( 'jquery' ), filemtime( get_parent_theme_file_path( 'js/theme.js' ) ), true );
	
}
add_action( 'wp_enqueue_scripts', 'djr_base_enqueue_scripts', 5 );

/**
 * Load stylesheets for the front end.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function djr_base_enqueue_styles() {
	wp_enqueue_style( 'font-awesome', get_parent_theme_file_uri( 'css/font-awesome.min.css' ), array(), '4.7.0' );
	// Load gallery style if 'cleaner-gallery' is active.
	if ( current_theme_supports( 'cleaner-gallery' ) )
		wp_enqueue_style( 'hybrid-gallery' );
	// Load parent theme stylesheet if child theme is active.
	if ( is_child_theme() )
		wp_enqueue_style( 'hybrid-parent' );
	// Load active theme stylesheet.
	wp_enqueue_style( 'hybrid-style' );
}
add_action( 'wp_enqueue_scripts', 'djr_base_enqueue_styles',  5 );

/**
 * adds a content/front.php to hybrid_content_template_hierarchy template part for front page
 *
 * @param $templates
 *
 * @return mixed
 */
function djr_base_add_hybrid_template( $templates ) {

	if ( is_front_page() && !is_home() ) {
		array_unshift( $templates , 'content/front.php' );
	}

	return $templates;

}
//add_filter( 'hybrid_content_template_hierarchy', 'djr_base_add_hybrid_template' );

/**
 * Function for deciding which pages should have a one-column layout.
 *
 * @since 1.0.0
 * @access public
 * @return void
 */
function djr_base_adjust_layout( $layout ) {

	/** for beaver builder */
	if ( ! in_array( $layout, array( '1c-b', '1c-l' ) ) && class_exists( 'FLBuilderModel' ) ) {

		$do_render = apply_filters( 'fl_builder_do_render_content', true, FLBuilderModel::get_post_id() );

		if ( ( $do_render && FLBuilderModel::is_builder_enabled() && ! is_archive() ) ||
		     ( FLBuilderModel::is_builder_active() && ! FLBuilderModel::current_user_has_editing_capability() )
		) {
			$layout = '1c-b';
		}
	}

	/** for single column */
		/** for single column */
		if (
			( is_attachment() && wp_attachment_is_image() ) ||
			( is_front_page() && ! is_home() && $layout != '1c-b' )
		) {

			$layout = '1c';

		}

	return $layout;

}
add_filter( 'hybrid_get_theme_layout', 'djr_base_adjust_layout', 15 );


/**
 * Disables sidebars if viewing a one-column page.
 *
 * @since 1.0.0
 * @access public
 * @param array $sidebars_widgets A multidimensional array of sidebars and widgets.
 * @return array $sidebars_widgets
 */
function djr_base_disable_sidebars( $sidebars_widgets ) {

	if ( current_theme_supports( 'theme-layouts' ) && !is_admin() ) {

		if ( in_array( hybrid_get_theme_layout(), array( '1c', '1c-b', '1c-l' ) ) ) {
			$sidebars_widgets['primary'] = false;
		}
	}

	return $sidebars_widgets;
}
add_filter( 'sidebars_widgets', 'djr_base_disable_sidebars' );