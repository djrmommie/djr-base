/* global require, process */
/* credit: https://github.com/FlagshipWP/compass */
module.exports = function( grunt ) {
	'use strict';

	// Load grunt plugins
	require( 'time-grunt' )(grunt);

	// Define project configuration
	var project = {
		paths: {
			get authorAssets() {
				return this.assets + 'theme/';
			},
			get bower() {
				return this.assets + 'bower/';
			},
			get composer() {
				return this.assets + 'composer/';
			},
			get config() {
				return this.grunt + 'config/';
			},
			get hybridCore() {
				return this.theme + 'library/';
			},
			get tasks() {
				return this.grunt + 'tasks/';
			},
			assets: '',
			dist: 'dist/',
            build: 'build/',
			docs: 'docs/',
			grunt: 'grunt/',
			languages: '/languages/',
			logs: 'logs/',
			theme: '../',
			tmp: 'tmp/'
		},
		files: {
			get php() {
				return project.paths.theme + '**/*.php';
			},
			get js() {
				return project.paths.assets + '{,*/}js/*.js';
			},
			get scss() {
				return project.paths.authorAssets + 'scss/**/*.scss';
			},
			get config() {
				return project.paths.config + '*.js';
			},
			get changelog() {
				return project.paths.theme + 'CHANGELOG.md';
			},
			grunt: 'Gruntfile.js'
		},
		pkg: grunt.file.readJSON( 'package.json' )
	};

	// Load Grunt plugin configurations
	require( 'load-grunt-config' )(grunt, {
		configPath: require( 'path' ).join( process.cwd(), project.paths.config ),
		data: project,
		jitGrunt: {
			staticMappings: {
				addtextdomain: 'grunt-wp-i18n',
				makepot: 'grunt-wp-i18n',
				wpcss: 'grunt-wp-css',
                usebanner: 'grunt-banner'
			},
			loadTasks: project.paths.tasks
		}
	});
};
