// https://github.com/gruntjs/grunt-contrib-watch
module.exports = {
	grunt: {
		options: {
			reload: true
		},
		files: [
			'<%= files.grunt %>',
			'<%= files.config %>'
		],
		tasks: [
			'jsvalidate:grunt'
		]
	},
	js: {
		options: {
			livereload: true
		},
		files: [
			'<%= files.js %>'
		],
		tasks: [
			'build:js',
			'jsvalidate:assets'
		]
	},
	scss: {
		options: {
			livereload: true
		},
		files: [
			'<%= files.scss %>'
		],
		tasks: [
			'sass',
			'postcss',
			'wpcss:css',
            'usebanner:theme',
			'cssmin:style',
			'replace:style',
			'copy:theme',
			'copy:css'
		]
	}
};
