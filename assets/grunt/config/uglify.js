// https://github.com/gruntjs/grunt-contrib-uglify
module.exports = {
	theme: {
		options: {
			sourceMap: false,
			mangle: true,
			compress: true,
			report: 'gzip'
		},
		files: [
			{
				expand: true,
				cwd: '<%= paths.theme %>js/',
				src: ['theme.js'],
				dest: '<%= paths.theme %>js/',
				ext: '.min.js',
				extDot: 'last'
			}
		]
	}
};
