// https://github.com/gruntjs/grunt-contrib-clean
module.exports = {
    options: {
        force: true
    },
	css: {
		src: [
			'<%= paths.theme %>css'
		]
	},
	logs: {
		src: [
			'<%= paths.logs %>'
		]
	},
	tmp: {
		src: [
			'<%= paths.tmp %>'
		]
	},
	js: {
		src: [
			'<%= paths.theme %>js'
		]
	},
	images: {
		src: [
			'<%= paths.theme %>images'
		]
	},
    fonts: {
        src: [
            '<%= paths.theme %>fonts'
        ]
    },
	hybridcore: {
		src: [
			'<%= paths.hybridCore %>'
		]
	},
	languages: {
		src: [
			'<%= paths.theme %>languages'
		]
	},
	style: {
		src: [
			'<%= paths.theme %>style*.*',
			'<%= paths.tmp %>style*.*'
		]
	},
	screenshot: {
		src: [
			'<%= paths.theme %>screenshot.png'
		]
	},
    build: {
        src: [
            '<%= paths.build %>'
        ]
    }

};
