// https://github.com/gruntjs/grunt-contrib-copy
module.exports = {
    css: {
        files: [
            {
                cwd: '<%= paths.tmp %>',
                expand: true,
                flatten: false,
                src: ['**/*.css','!style*.css', '!style*.map'],
                dest: '<%= paths.theme %>/css',
                filter: 'isFile'
            }
        ]
    },
    cssAssets: {
        files: [
            {
                cwd: '<%= paths.bower %>',
                expand: true,
                flatten: true,
                src: ['font-awesome/css/font-awesome.min.css'],
                dest: '<%= paths.theme %>css/',
                filter: 'isFile'
            }
        ]
    },
    csstoscss: {
        files: [
            {
                cwd: '<%= paths.bower %>',
                expand: true,
                dot: true,
                flatten: true,
                src: ['sanitize-css/*.css'],
                dest: '<%= paths.authorAssets %>scss/vendor/',
                filter: 'isFile',
                rename: function(dest, src) {
                    return dest + '_' + src.replace('.css','.scss');
                }
            }
        ]
    },
    scss: {
        files: [
            {
                cwd: '<%= paths.bower %>',
                expand: true,
                dot: true,
                flatten: true,
                src: ['sass-mq/*.scss'],
                dest: '<%= paths.authorAssets %>scss/vendor/',
                filter: 'isFile'
            }
        ]
    },
    theme: {
        files: [
            {
                cwd: '<%= paths.tmp %>',
                expand: true,
                flatten: false,
                src: ['style*.css', 'style*.map'],
                dest: '<%= paths.theme %>',
                filter: 'isFile'
            }
        ]
    },
    js: {
        files: [
            {
                cwd: '<%= paths.authorAssets %>js/',
                expand: true,
                flatten: false,
                src: ['*'],
                dest: '<%= paths.theme %>js/',
                filter: 'isFile'
            }
        ]
    },
    jsAssets: {
        files: [
            {
                cwd: '<%= paths.bower %>',
                expand: true,
                flatten: true,
                src: ['html5shiv/html5shiv.min.js','flexibility/flexibility.js','ofi/ofi.min.js'],
                dest: '<%= paths.theme %>js/',
                filter: 'isFile'
            }
        ]
    },
    fonts: {
        files: [
            {
                cwd: '<%= paths.bower %>',
                expand: true,
                flatten: true,
                src: ['font-awesome/fonts/*'],
                dest: '<%= paths.theme %>fonts/',
                filter: 'isFile'
            }
        ]
    },
    hybridcore: {
        files: [
            {
                cwd: '<%= paths.composer %>justintadlock/hybrid-core',
                expand: true,
                src: ['**/*'],
                dest: '<%= paths.hybridCore %>'
            }
        ]
    },
    images: {
        files: [
            {
                cwd: '<%= paths.tmp %>images',
                expand: true,
                flatten: false,
                src: ['**/*', '!screenshot.png'],
                dest: '<%= paths.theme %>images',
                filter: 'isFile'
            }
        ]
    },
    screenshot: {
        files: [
            {
                cwd: '<%= paths.tmp %>images',
                expand: true,
                flatten: true,
                src: ['screenshot.png'],
                dest: '<%= paths.theme %>',
                filter: 'isFile'
            }
        ]
    },
    languages: {
        files: [
            {
                cwd: '<%= paths.assets %><%= paths.languages %>',
                expand: true,
                src: ['*.po'],
                dest: '<%= paths.theme%><%= paths.languages %>',
                filter: 'isFile'
            }
        ]
    },
    licenses: {
        files: [
            {
                cwd: '<%= paths.bower %>',
                expand: true,
                src: ['*/*LICENSE*','*/*license*'],
                dest: '<%= paths.theme%><%= paths.docs %>',
                filter: 'isFile'
            }
        ]
    },
    build: {
        expand: true,
        cwd: '<%= paths.theme %>',
        src: ['**', '!dist/**', '!readme.md', '!.gitignore', '!assets/**'],
        dest: '<%= paths.build %>'
    }
};
