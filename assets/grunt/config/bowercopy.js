// https://github.com/timmywil/grunt-bowercopy
module.exports = {
    options: {
        clean: true,
        ignore: ['jquery']
    },
    css: {
        options: {
            destPrefix: '<%= paths.bower %>'
        },
        files: {
            'font-awesome': 'font-awesome',
            'sanitize-css': 'sanitize-css',
            'sass-mq': 'sass-mq'
        }
    },
    js: {
        options: {
            destPrefix: '<%= paths.bower %>'
        },
        files: {
            'html5shiv': ['html5shiv/dist/html5shiv.min.js','html5shiv/*.md'],
            'flexibility': ['flexibility/flexibility.js','flexibility/*.md'],
            'ofi': ['object-fit-images/dist/ofi.min.js','object-fit-images/license']
        }
    }
};
