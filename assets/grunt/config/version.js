// https://github.com/gruntjs/grunt-contrib-compress
module.exports = {
    project: {
        src: ['package.json', 'bower.json']
    },
    php: {
        options: {
            prefix: '\@version\\s+'
        },
        src: [ '../functions.php' ]
    }
}