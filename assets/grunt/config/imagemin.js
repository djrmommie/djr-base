// https://github.com/gruntjs/grunt-contrib-imagemin
module.exports = {
	assets: {
        options: {
            pngquant: true
        },
		files: [
			{
				expand: true,
				cwd: '<%= paths.authorAssets %>images',
                src: ['**/*'],
				dest: '<%= paths.tmp %>images'
			}
		]
	}
};
