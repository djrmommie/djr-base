// https://github.com/gruntjs/grunt-contrib-compress
module.exports = {
    build: {
        options: {
            archive: '<%= paths.dist %>/<%= pkg.name %>-<%= pkg.version %>.zip'
        },
        expand: true,
        cwd: '<%= paths.build %>/',
        src: '**',
        dest: '<%= pkg.name %>/'
    }
}