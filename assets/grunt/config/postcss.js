// https://github.com/nDmitry/grunt-postcss
module.exports = {
	options: {
        processors: [
            require('autoprefixer')({browsers: ['last 2 versions']}),
            require('postcss-flexibility')
        ],
		map: false
	},
    theme: {
        options: {
            map: true
        },
        src: '<%= paths.tmp %>style.css',
        dest: '<%= paths.tmp %>style.css'
    },
    editorstyle: {
        src: '<%= paths.tmp %>editor-style.css',
        dest: '<%= paths.tmp %>editor-style.css'
    }
};
