DJR Base Theme Assets
===================

## Requirements

Node.js
Bower
Composer

## Get started
- npm install inside assets folder
- run grunt build
- run grunt (default) to watch for sass updates

## Customize for new theme (before grunt build or rerun grunt build after)
- assets/package.json
- functions.php
- assets/composer.json
- find and replace for "djr_base", "djr-base"

## Updating dependencies
- npm-check-updates for checking and updating versions for node modules and bower https://www.npmjs.com/package/npm-check-updates
  - `ncu` to check package.json
  - `ncu -u` to upgrade package.json
  - `ncu -m bower` to check brower.json
  - `ncu -m bower -u` to upgrade bower.json
- Use `composer update` command to update composer packages

## Copyright and License

See additional copyright/licenses in djr-base/readme.md

* Build tools (grunt, bower, composer) configuration based on FlagshipWP Compass project (no longer available), Rob Neu <https://github.com/robneu> and Gary Jones <http://gamajo.com>
* assets/theme/scss is based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc.
* [sass-mq](https://github.com/sass-mq/sass-mq) Copyright (c) 2013-2016 Guardian Media Group and contributors - [MIT License](http://opensource.org/licenses/MIT).