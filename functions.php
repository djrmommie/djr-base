<?php
/**
 * “Failures, repeated failures, are finger posts on the road to achievement. One fails forward towards success.”
 * ~ C.S. Lewis
 *
 * Theme Authors: Make sure to add a favorite quote of yours above, maybe something that inspired you to
 * create this theme.
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write
 * to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Based on HybridBase, Copyright (c) 2013 - 2017, Justin Tadlock  <justin@justintadlock.com> http://themehybrid.com/themes/hybrid-base
 *
 * @package    DJRBase
 * @version    2.0.0
 * @author     Jenny Ragan <jenny@jennyragan.com>
 * @copyright  Copyright (c) 2017, Jenny Ragan
 * @link       https://bitbucket.org/djrmommie/djr-base
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/**
 * Singleton class for launching the theme and setup configuration.
 *
 * @since  1.0.0
 * @access public
 */
final class Djr_Base_Theme {

	/**
	 * Directory path to the theme folder.
	 *
	 * @since  1.0.0
	 * @access public
	 * @var    string
	 */
	public $dir_path = '';

	/**
	 * Directory URI to the theme folder.
	 *
	 * @since  1.0.0
	 * @access public
	 * @var    string
	 */
	public $dir_uri = '';

	/**
	 * Returns the instance.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return object
	 */
	public static function get_instance() {

		static $instance = null;

		if ( is_null( $instance ) ) {
			$instance = new self;
			$instance->setup();
			$instance->includes();
			$instance->setup_actions();
		}

		return $instance;
	}

	/**
	 * Constructor method.
	 *
	 * @since  1.0.0
	 * @access private
	 * @return void
	 */
	private function __construct() {}

	/**
	 * Prevent cloning
	 *
	 * @since  1.0
	 * @return void
	 */
	public function __clone() {
		// Cloning instances of the class is forbidden
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'djr-base' ), '1.0' );
	}
	/**
	 * Prevent unserializing
	 *
	 * @since  1.0
	 * @return void
	 */
	public function __wakeup() {
		// Unserializing instances of the class is forbidden.
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'djr-base' ), '1.0' );
	}

	/**
	 * Initial theme setup.
	 *
	 * @since  1.0.0
	 * @access private
	 * @return void
	 */
	private function setup() {

		$this->dir_path = trailingslashit( get_template_directory() );
		$this->dir_uri  = trailingslashit( get_template_directory_uri() );
	}

	/**
	 * Loads include and admin files for the plugin.
	 *
	 * @since  1.0.0
	 * @access private
	 * @return void
	 */
	private function includes() {

		// Load the Hybrid Core framework and theme files.
		require_once( $this->dir_path . 'library/hybrid.php' );

		// Load theme includes.
		require_once( $this->dir_path . 'inc/custom-background.php' );
		require_once( $this->dir_path . 'inc/custom-header.php'     );
		require_once( $this->dir_path . 'inc/functions-filters.php'             );

		// Launch the Hybrid Core framework.
		//new Hybrid();
	}

	/**
	 * Sets up initial actions.
	 *
	 * @since  1.0.0
	 * @access private
	 * @return void
	 */
	private function setup_actions() {

		// Theme setup.
		add_action( 'after_setup_theme', array( $this, 'theme_setup' ),  5 );
		add_action( 'after_setup_theme', array( $this, 'custom_background_setup' ), 15 );

		// Register menus.
		add_action( 'init', array( $this, 'register_menus' ) );

		// Register image sizes.
		add_action( 'init', array( $this, 'register_image_sizes' ) );

		// Register layouts.
		add_action( 'hybrid_register_layouts', array( $this, 'register_layouts' ) );
	}

	/**
	 * The theme setup function.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function theme_setup() {

		// Theme layouts.
		add_theme_support(
			'theme-layouts',
			array( 'default' => is_rtl() ? '2c-r' :'2c-l' )
		);

		// Enable custom template hierarchy.
		add_theme_support( 'hybrid-core-template-hierarchy' );

		// The best thumbnail/image script ever.
		add_theme_support( 'get-the-image' );

		// Breadcrumbs. Yay!
		add_theme_support( 'breadcrumb-trail' );

		// Nicer [gallery] shortcode implementation.
		add_theme_support( 'cleaner-gallery' );

		// Automatically add feed links to <head>.
		add_theme_support( 'automatic-feed-links' );

		// Post formats.
		add_theme_support(
			'post-formats',
			array( 'aside', 'audio', 'chat', 'image', 'gallery', 'link', 'quote', 'status', 'video' )
		);

		// Handle content width for embeds and images.
		hybrid_set_content_width( 1280 );
	}

	/**
	 * Adds support for the WordPress 'custom-background' theme feature.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function custom_background_setup() {

		add_theme_support(
			'custom-background',
			array(
				'default-color'    => 'ffffff',
				'default-image'    => '',
				'wp-head-callback' => 'djr_base_custom_background_callback',
			)
		);
	}

	/**
	 * Registers nav menus.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function register_menus() {

		register_nav_menu( 'primary',    esc_html_x( 'Primary',    'nav menu location', 'djr-base' ) );
		register_nav_menu( 'secondary',  esc_html_x( 'Secondary',  'nav menu location', 'djr-base' ) );
		register_nav_menu( 'subsidiary', esc_html_x( 'Subsidiary', 'nav menu location', 'djr-base' ) );
	}

	/**
	 * Registers image sizes.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function register_image_sizes() {

		// Landscape sizes.
		//set_post_thumbnail_size( 600, 400, true );
		//add_image_size( 'djr-base-size', 150, 150, true );
	}

	/**
	 * Registers layouts.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function register_layouts() {

		hybrid_register_layout( '1c',   array( 'label' => esc_html__( '1 Column',                     'djr-base' ), 'image' => '%s/images/layouts/1c.png'   ) );
		hybrid_register_layout( '2c-l', array( 'label' => esc_html__( '2 Columns: Content / Sidebar', 'djr-base' ), 'image' => '%s/images/layouts/2c-l.png' ) );
		hybrid_register_layout( '2c-r', array( 'label' => esc_html__( '2 Columns: Sidebar / Content', 'djr-base' ), 'image' => '%s/images/layouts/2c-r.png' ) );
		hybrid_register_layout( '1c-b', array( 'label' => esc_html__( 'Page Builder', 'djr-base' ), 'image' => '%s/images/layouts/1c.png' ) );
		hybrid_register_layout( '1c-l', array( 'label' => esc_html__( 'Landing Page', 'djr-base' ), 'image' => '%s/images/layouts/1c.png' ) );

	}
}

/**
 * Gets the instance of the `Djr_base_Theme` class.
 *
 * @since  1.0.0
 * @access public
 * @return object
 */
function djr_base_theme() {
	return Djr_Base_Theme::get_instance();
}

// run
djr_base_theme();
