# DJR Base

Starter theme for the [Hybrid Core framework](http://themehybrid.com/hybrid-core).

## Use

See assets/readme.md

## Copyright and License

DJRBase is based on Hybrid Base http://themehybrid.com/themes/hybrid-base,  Copyright (c) 2013 - 2016, Justin Tadlock

The following resources are included within the theme package.

* [Hybrid Core Framework](http://themehybrid.com/hybrid-core) by Justin Tadlock - [GPL  2+](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html).
* [Font Awesome](http://fontawesome.io) by Dave Gandy - Font licensed under the [SIL Open Font License, version 1.1](http://scripts.sil.org/OFL) and CSS licensed under the [MIT License](http://opensource.org/licenses/mit-license.html).
* [Flexibility](https://github.com/jonathantneal/flexibility) Copyright (c)) 2015 Jonathan Neal, 10up - [MIT License](http://opensource.org/licenses/MIT).
* [HTML5 Shiv](https://github.com/aFarkas/html5shiv) Copyright (c) 2014 Alexander Farkas (aFarkas) - [MIT License](http://opensource.org/licenses/MIT)/[GPL2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html).
* [sanitize.css](https://github.com/jonathantneal/sanitize.css) is dedicated to the public domain [CC0](https://creativecommons.org/publicdomain/zero/1.0/).
* [object-fit-images](https://github.com/bfred-it/object-fit-images) (c) Federico Brigante <bfred-it@users.noreply.github.com> (twitter.com/bfred_it) - [MIT License](http://opensource.org/licenses/MIT).


All other resources and theme elements are licensed under the [GNU GPL](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html), version 2 or later.

2016 &copy; [Justin Tadlock](http://justintadlock.com).